@extends('layouts.app')
@include('includes.header')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Note</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-success float-right" href="{{ route('notes.create') }}"> Create New Note</a>
                </div>
            </div>
        </div>

        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif

        <table class="table table-bordered">
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Details</th>
                <th width="280px">Action</th>
            </tr>
            @foreach ($notes as $note)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $note->title }}</td>
                    <td>{{ $note->note }}</td>
                    <td>
                        <form action="{{ route('notes.destroy',$note->id) }}" method="POST">

                            <a class="btn btn-info" href="{{ route('notes.show',$note->id) }}">Show</a>

                            <a class="btn btn-primary" href="{{ route('notes.edit',$note->id) }}">Edit</a>

                            @csrf
                            @method('DELETE')

                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>

        {!! $notes->links() !!}
    </div>


@endsection
