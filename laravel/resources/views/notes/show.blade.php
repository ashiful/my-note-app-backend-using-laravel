@extends('layouts.app')
@include('includes.header')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2> Show Note</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('notes.index') }}"> Back</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Title:</strong>
                    {{ $note->title }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Note:</strong>
                    {{ $note->note }}
                </div>
            </div>
        </div>
    </div>

@endsection
