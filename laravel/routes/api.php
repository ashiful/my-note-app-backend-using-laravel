<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::group([

    'middleware' => 'api','auth',
    'prefix' => 'auth',


], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');


});


Route::group([

    'middleware' => 'api','auth',

], function ($router) {



});*/

Route::post('auth/login', 'Api\\LoginController@login');
Route::apiResource('notes', 'Api\\NoteController');

