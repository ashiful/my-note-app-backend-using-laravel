<?php

namespace App\Http\Controllers;

use App\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NoteController extends Controller
{

    public function index()
    {
        $id = auth()->user()->id;
        $notes = Note::where('user_id', $id)->latest()->paginate(10);

        return view('notes.index', compact('notes'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function create()
    {
        return view('notes.create');
    }

    public function store(Request $request)
    {


        $request->validate([
            'user_id' => 'required',
            'title' => 'required',
            'note' => 'required',
        ]);

        Note::create($request->all());

        return redirect()->route('notes.index')
            ->with('success', 'Note created successfully.');
    }

    public function show(Note $note)
    {
        return view('notes.show', compact('note'));
    }

    public function edit(Note $note)
    {
        return view('notes.edit', compact('note'));
    }

    public function update(Request $request, Note $note)
    {
        $request->validate([
            'user_id' => 'required',
            'title' => 'required',
            'note' => 'required',
        ]);

        $note->update($request->all());

        return redirect()->route('notes.index')
            ->with('success', 'Note updated successfully');
    }

    public function destroy(Note $note)
    {
        $note->delete();

        return redirect()->route('notes.index')
            ->with('success', 'Note deleted successfully');
    }
}
