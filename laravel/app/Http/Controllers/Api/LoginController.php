<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);

        if (auth()->attempt($credentials)) {
            $user = auth()->user();
            return $user;
        } else {
            return false;
        }
    }
}
