<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
 
class UserController extends Controller
{

    public function index()
    {
        $users = User::latest()->paginate(5);

        return view('users.index', compact('users'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('users.create');
    }

    public function store(Request $request)
    {



        $request->validate([
            'user_id' => 'required',
            'title' => 'required',
            'note' => 'required',
        ]);

        User::create($request->all());

        return redirect()->route('users.index')
            ->with('success', 'User created successfully.');
    }

    public function show(User $note)
    {
        return view('users.show', compact('note'));
    }

    public function edit(User $note)
    {
        return view('users.edit', compact('note'));
    }

    public function update(Request $request, User $note)
    {
        $request->validate([
            'user_id' => 'required',
            'title' => 'required',
            'note' => 'required',
        ]);

        $note->update($request->all());

        return redirect()->route('users.index')
            ->with('success', 'User updated successfully');
    }

    public function destroy(User $note)
    {
        $note->delete();

        return redirect()->route('users.index')
            ->with('success', 'User deleted successfully');
    }
}
